import sys


class Reader:
    def readline(self):
        '''
        Wczytujemy jedna linie, bez znaku konca linii,
        gdy nie bedzie wiecej danych rzucamy EOFError.
        '''
        raise NotImplementedError
        
    def close(self):
        pass
    
        
class ConsoleReader(Reader):
    def readline(self):
        return input()

    
class FileReader(Reader):
    def __init__(self, filename):
        self.file_to_read = open(filename)

    def readline(self):
        line = self.file_to_read.readline()
        if line == '':
            raise EOFError
        
        line = line[:-1]
        
        return line
        
    def close(self):
        self.file_to_read.close()
        

def uniq(reader):
    try:
        last_line = ''
        while True:
            line = reader.readline()

            if line != last_line:
                print(line)
                last_line = line
    except EOFError:
        pass
    
    
def main():
    #print('Argumenty:', sys.argv)
    
    should_read_from_file = (len(sys.argv) == 2)
    if should_read_from_file:
        reader = FileReader('tekst.txt')
    else:
        reader = ConsoleReader()
    
    uniq(reader)
    
    reader.close()
    
    
if __name__ == '__main__':
    main()
    