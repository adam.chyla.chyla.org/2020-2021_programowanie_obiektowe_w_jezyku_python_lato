class ReplApplication:
    def __init__(self):
        self.should_quit = False
    
    def run(self):
        self.on_start()

        while self.should_quit is False:
            self.on_prompt()
            
            text = input()
            
            self.on_input(text)

    def on_start(self):
        pass
    
    def on_prompt(self):
        pass
    
    def on_input(self, text):
        pass
    
    
class MyApp(ReplApplication):
    def on_start(self):
        print("Welcome!")
        
    def on_prompt(self):
        print(">>> ", end='')
        
    def on_input(self, text):
        if text == "exit":
            self.should_quit = True
        else:
            do_action(text)

def main():
    app = MyApp()
    app.run()
    
        
def do_action(text):
    print("action:", text)
    
if __name__ == "__main__":
    main()
    