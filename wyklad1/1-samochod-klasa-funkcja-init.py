
class Samochod:

    # metoda - funkcja wewnatrz klasy
    def init(self, kolor, marka):      # PEP8
        self.kolor = kolor
        self.marka = marka


s1 = Samochod()
s1.init('czerwony', 'fiat')

s2 = Samochod()
s2.init('czarny', 'bmw')


def run(sam):
    print("Jedziemy...", sam.kolor, sam.marka)
    
run(s1)
run(s2)

