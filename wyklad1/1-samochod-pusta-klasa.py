
class Samochod:
    pass

def init(sam, kolor, marka):
    sam.kolor = kolor
    sam.marka = marka

    
s1 = Samochod()
init(s1, 'czerwony', 'fiat')


s2 = Samochod()
init(s2, 'czarny', 'bmw')


def run(sam):
    print("Jedziemy...", sam.kolor, sam.marka)
    
run(s1)
run(s2)

