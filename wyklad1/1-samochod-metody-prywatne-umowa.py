class Samochod:

    # metody magiczne: https://www.tutorialsteacher.com/python/magic-methods-in-python
    # metoda - funkcja wewnatrz klasy
    def __init__(self, kolor, marka):      # PEP8
        self.kolor = kolor
        self.marka = marka
        self._stan_paliwa = 0              # PEP8 - jeden znak podlogi umowa

    def __str__(self):
        return "Samochod: kolor={}; marka={};".format(self.kolor, 
                                                      self.marka)

    def zatankuj(self, ile):
        self._stan_paliwa += ile

    def run(self):
        if self._stan_paliwa > 10:
            print("Jedziemy...", self.kolor, self.marka)
        else:
            print("Za malo paliwa.")


s1 = Samochod('czerwony', 'fiat')
s2 = Samochod('czarny', 'bmw')

print(s1)  # ->   print( str(s1) ) --> print( s1.__str__() )

s1.zatankuj(12)
s2.zatankuj(15)

s1._stan_paliwa = 0

s1.run()
s2.run()

