
class Samochod:

    # metody magiczne: https://www.tutorialsteacher.com/python/magic-methods-in-python
    # metoda - funkcja wewnatrz klasy
    def __init__(self, kolor, marka):      # PEP8
        self.kolor = kolor
        self.marka = marka

    def __str__(self):
        return "Samochod: kolor={}; marka={};".format(self.kolor, 
                                                      self.marka)

    def run(self):
        print("Jedziemy...", self.kolor, self.marka)


s1 = Samochod('czerwony', 'fiat')
s2 = Samochod('czarny', 'bmw')

print(s1)  # ->   print( str(s1) ) --> print( s1.__str__() )
  
s1.run()
s2.run()

