#to jest w pythonie
#class BaseException:
#    ...
#
#class Exception(BaseException):
#    ...
#
#class ValueError(Exception):
#    ...
#
    
class PelnyBakError(Exception):
    def __init__(self, ile):
        msg = f"proba zatankowania: {ile}"
        super().__init__(msg)



# klasa bazowa
class Samochod:
    # metody magiczne: https://www.tutorialsteacher.com/python/magic-methods-in-python
    # metoda - funkcja wewnatrz klasy
    def __init__(self, kolor, marka):      # PEP8
        self.kolor = kolor
        self.marka = marka
        self._stan_paliwa = 0             # PEP8

    def __str__(self):
        return "Samochod: kolor={}; marka={};".format(self.kolor, 
                                                      self.marka)

    def zatankuj(self, ile):
        self._stan_paliwa += ile

    def run(self):
        if self._czy_odpowiednia_ilosc_paliwa():
            print("Jedziemy...", self.kolor, self.marka)
        else:
            print("Za malo paliwa.")

    def _czy_odpowiednia_ilosc_paliwa(self):
        return self._stan_paliwa > 10


class SamochodLpg(Samochod):
    def __init__(self, kolor, marka): 
        super().__init__(kolor, marka)
        self._stan_lpg = 0

    def _czy_odpowiednia_ilosc_paliwa(self):
        return self._stan_paliwa > 20 or self._stan_lpg > 40

    def zatankuj_lpg(self, ile):
        if self._czy_mozna_zatankowac(ile):
            self._stan_lpg += ile
        else:
            raise PelnyBakError(ile)
        
    def _czy_mozna_zatankowac(self, ile):
        return ile + self._stan_lpg < 100


    
s1 = Samochod('czerwony', 'fiat')
s2 = SamochodLpg('czarny', 'bmw')

try:
    s1.zatankuj(12)
    s2.zatankuj(15)
    s2.zatankuj_lpg(150)
except Exception as e:
    print("Blad:", e)

    
s1.run()
s2.run()
