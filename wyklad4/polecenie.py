import time

class ProgressCommand:
    def execute(self):
        raise NotImplementedError('you should implement this method in subclass')


def progress(commands):
    for i, cmd in enumerate(commands, start=1):
        l_znakow = int(i / len(commands) * 10)
        zp = '#' * l_znakow
        zb = ' ' * (10 - l_znakow)
        print("|" + zp + zb + "|\r", end='')

        cmd.execute()

        time.sleep(1)

    print()


class AppendNumbersToList(ProgressCommand):
    def __init__(self, lista):
        self.lista = lista
        self.counter = 0
        
    def execute(self):
        self.lista.append(self.counter)
        
        self.counter += 1


def main():
    liczby = []

    append_cmd = AppendNumbersToList(liczby)
    # mozemy dodawac kolejne polecenia
    #other_cmd = OtherCmd()
    
    progress([append_cmd, 
              append_cmd,
              #other_cmd,
              append_cmd, 
              append_cmd, 
              #other_cmd,
              append_cmd, 
              append_cmd, 
              append_cmd, 
              append_cmd, 
              append_cmd, 
              append_cmd, 
              append_cmd, 
              append_cmd])
    
    print(liczby)
    
if __name__ == '__main__':
    main()
